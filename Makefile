all:
	pdflatex review.tex
	bibtex review.aux
	pdflatex review.tex
	pdflatex review.tex
	rm -f review.aux review.log review.bbl review.blg review.out

clean:
	rm -f review.pdf
